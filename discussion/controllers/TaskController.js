const Task = require('../models/Task.js');

module.exports.getAllTasks = () =>{
    //business logic
    return Task.find({})
    .then(results => {
        return results
    })
};


module.exports.createTask = (reqBody) => {
    let newTask = new Task ({
        name: reqBody.name

    }) 
    return newTask.save().then((savedTask, error) => {
        if(error){
            console.log(error)
            return false
        } else if (savedTask != null && savedTask.name == reqBody.name) {
                    return 'Duplicate task found!'
        } else {
            return savedTask  
            //or u can use string message: "Task Created successfully"
        }
    })
};



module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId)
    .then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.name = newContent.name

			return result.save()
            .then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
};

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId)
    .then((deletedTask, error)=> {
        if(error) {
            console.log(error)
            return false
        } else {
            return deletedTask // or 'task deleted successfully'
        }
    })

};
