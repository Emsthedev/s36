
const express = require('express');
const TaskController = require('../controllers/TaskController.js')

//Creates a Router instance that functions as a middleware and routing system
const router = express.Router();

//Routes
router.get('/:id', (req,res) => {
    TaskController.getOneTasks()
    .then((resultFromController) => res.send(resultFromController))
 
 })


//Routes for creating tasks
router.post('/create',(req,res) => {
TaskController.createTask(req.body)
.then((resultFromController) => res.send(resultFromController))

})

router.put('/:id/complete',(req,res) => {
    TaskController.updateTask(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController))
    
    })


  

module.exports = router