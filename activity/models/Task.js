const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
    //defines the fields/object, with the corresponding data types
    //for a Task it needs a "task name" data type is String
    name: String,
    //field called "status" data type is String, default value is Pending
    status: {
        type: String,
        default: 'Pending'
    }
})

// module.exports is a way for NodeJS to treat as a 'pacakge' that can be used by other files
module.exports = mongoose.model('Task', taskSchema)

//const Task = mongoose.model('Task', taskSchema)