
const express = require('express');
const TaskController = require('../controllers/TaskController.js')

//Creates a Router instance that functions as a middleware and routing system
const router = express.Router();

//Routes
router.get('/', (req,res) => {
   TaskController.getAllTasks()
   .then((resultFromController) => res.send(resultFromController))

})
//Routes for creating tasks
router.post('/create',(req,res) => {
TaskController.createTask(req.body)
.then((resultFromController) => res.send(resultFromController))

})

router.put('/:id/update',(req,res) => {
    TaskController.updateTask(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController))
    
    })


    router.delete('/:id/delete',(req,res) => {
        TaskController.deleteTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        
        })


module.exports = router