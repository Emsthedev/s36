//EXPRESS SERVER SETUP
const express = require('express');
// Mongoose is a package that allows the ceation of schemas to model
//our data structures
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes.js')

const app = express();
const port = 3001;

//middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//establishing connection mongodb
//[Section] - Mongoose Connection
//Mongoose uses the 'connect ' func to connect to the cluster in our MondoDB Atlas
/* 
It takes 2 arguments:
1.connection string from our mondodb atlas
2.obect that conatins the middlewares/standars that Mondo DB uses
*/

mongoose.connect(`mongodb+srv://root:root@zuitt-b197pt.xjoh3wx.mongodb.net/S36-discussion?retryWrites=true&w=majority`,{
   //it allows us to avoid any 
   //current and/or future error while connecting to Mongo DB
    useNewUrlParser : true,
    //if false :unifiedtopology by default, we need to set it to true to opt in
    //to using the MongoDB driver's new connection managment engine.
    useUnifiedTopology : true
})
//initializes the mongoose connection to the MongoDB by 
//assigning mongoose.connection to the 'db' variable
let db = mongoose.connection

/* 
Listener events of the connection by using the .on() function
of the mongoose connection and logs the details in 
the console based of the event (error or successful)
 */
db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'));

//middleware
//http://localhost:3001/tasks/(/URI)
app.use('/tasks', taskRoutes)



app.listen(port, () => console.log(`Server is running at port ${port}`))










//CREATING SCHEMA

/* const taskSchema = new mongoose.Schema({
    //defines the fields/object, with the corresponding data types
    //for a Task it needs a "task name" data type is String
    name: String,
    //field called "status" data type is String, default value is Pending
    status: {
        type: String,
        default: 'Pending'
    }
}) */

//MODELS

/*  */
//the variable/object "Task" can now be used to run commands for interacting with our DB
//"Task" is capitalized following the MVC approach for naming convention
//Models must be in singular form and capitalized
//The 1st parameter is used to specify the Name of the collection
//where we will store our data
//the 2nd is used to spcify the schema/blueprint of the dpcuments
//that will be stored in our MongoDB collection/s
//  
// const Task = mongoose.model('Task', taskSchema)




//CREATING Routes


/* app.get('/tasks', (req,res) => {
    Task.find({}, (error, result) => {
        if(error){
            return res.send(error)
        } else{
            return res.status(200).json({
                tasks : result
            })
        }
    })
}) */


// app.listen(port, () => console.log(`Server is running at port ${port}`))

//End of Express Setup

 




