const Task = require('../models/Task.js');

module.exports.getOneTasks = () =>{
    //business logic
    return Task.findOne({})
    .then((result, error) => {
        if(error) {
            console.log(error)
            return false
        } else {
            return result
            
        }
        })
};


module.exports.createTask = (reqBody) => {
    let newTask = new Task ({
        name: reqBody.name

    }) 
    return newTask.save().then((savedTask, error) => {
        if(error){
            console.log(error)
            return false
        } else if (savedTask != null && savedTask.name == reqBody.name) {
                    return 'Duplicate task found!'
        } else {
            return savedTask  
            //or u can use string message: "Task Created successfully"
        }
    })
};



module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId)
    .then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.status = newContent.status

			return result.save()
            .then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
};

